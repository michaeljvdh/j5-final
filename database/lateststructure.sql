-- --------------------------------------------------------
-- Host:                         192.168.0.42
-- Server version:               10.1.41-MariaDB-0ubuntu0.18.04.1 - Ubuntu 18.04
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------
USE jayfish;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table jayfish.admin
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL,
  `setting` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table jayfish.admin: ~9 rows (approximately)
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT IGNORE INTO `admin` (`id`, `setting`, `value`) VALUES
  (0, 'wallpaper', 'wallpaper.jpg'),
  (3, 'passcode', '123'),
  (4, 'feed', '0'),
  (5, 'webcam-int', '192.168.0.14:9999'),
  (6, 'webcam-ext', '70.79.91.189:9999'),
  (7, 'webcam-sub', '192'),
  (8, 'dashpark', '300'),
  (9, 'reboot', '0'),
  (10, 'shutdown', '0');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- Dumping structure for table jayfish.alert
CREATE TABLE IF NOT EXISTS `alert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection` longtext,
  `category` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `message` longtext,
  `dateset_timeset` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sendmessage` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3156 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.atlas
CREATE TABLE IF NOT EXISTS `atlas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor` varchar(50) DEFAULT NULL,
  `reading` float DEFAULT NULL,
  `dateset` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.ato_relay
CREATE TABLE IF NOT EXISTS `ato_relay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL DEFAULT '0',
  `value` varchar(50) NOT NULL DEFAULT '0',
  `gpio` varchar(50) NOT NULL DEFAULT '0',
  `switchgpio` varchar(50) NOT NULL DEFAULT '0',
  `ml` int(11) NOT NULL DEFAULT '0',
  `polarity` int(11) NOT NULL DEFAULT '0',
  `failswitchgpio` int(11) NOT NULL DEFAULT '0',
  `resevoirgpio` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.chem
CREATE TABLE IF NOT EXISTS `chem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shortname` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `measurement` varchar(45) NOT NULL,
  `threshold` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shortname` (`shortname`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.classification
CREATE TABLE IF NOT EXISTS `classification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classification` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.codes
CREATE TABLE IF NOT EXISTS `codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.dhtconfig
CREATE TABLE IF NOT EXISTS `dhtconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` int(11) DEFAULT '15',
  `relaygpio` int(11) DEFAULT '0',
  `tpivot` int(11) DEFAULT '0',
  `hpivot` int(11) DEFAULT '0',
  `relaystate` int(11) DEFAULT '0',
  `pwmrate` int(11) DEFAULT '0',
  `pwmoveride` int(11) DEFAULT '0',
  `pwmpenalty` int(11) DEFAULT '0',
  `dhtgpio` int(11) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.dhtlog
CREATE TABLE IF NOT EXISTS `dhtlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tdht` int(11) DEFAULT NULL,
  `hdht` int(11) DEFAULT NULL,
  `tstamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pwmpercent` int(11) DEFAULT NULL,
  `dhtconfig_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.event
CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` varchar(50) NOT NULL DEFAULT '0',
  `dateset` date DEFAULT NULL,
  `timeset` time DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL,
  `filterid` int(11) DEFAULT NULL,
  `chemid` int(11) DEFAULT NULL,
  `value_1` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3899 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.filter
CREATE TABLE IF NOT EXISTS `filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shortname` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `expiry` int(11) DEFAULT NULL,
  `dateset` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.generic_devices
CREATE TABLE IF NOT EXISTS `generic_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `gpio` int(11) DEFAULT '0',
  `ledgpio` int(11) DEFAULT '0',
  `state` varchar(50) DEFAULT NULL,
  `polarity` int(11) DEFAULT '0',
  `pulse` int(11) DEFAULT '0',
  `pulsetime` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.help
CREATE TABLE IF NOT EXISTS `help` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(50) DEFAULT NULL,
  `object` varchar(50) DEFAULT NULL,
  `message` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.inhab_category
CREATE TABLE IF NOT EXISTS `inhab_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.inhab_species
CREATE TABLE IF NOT EXISTS `inhab_species` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateset` timestamp NULL DEFAULT NULL,
  `inhab_category_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `latin` varchar(50) DEFAULT NULL,
  `description` longtext,
  `image` varchar(50) DEFAULT NULL,
  `date_introduced` timestamp NULL DEFAULT NULL,
  `inhab_status_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1_inhab_species_category` (`inhab_category_id`),
  CONSTRAINT `FK1_inhab_species_category` FOREIGN KEY (`inhab_category_id`) REFERENCES `inhab_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.inhab_status
CREATE TABLE IF NOT EXISTS `inhab_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.ledim
CREATE TABLE IF NOT EXISTS `ledim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ledim_name_id` int(11) NOT NULL,
  `start` int(11) NOT NULL,
  `end` int(11) NOT NULL,
  `speed` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `phase` varchar(50) NOT NULL,
  `channel` int(11) NOT NULL,
  `auto` int(11) NOT NULL,
  `manual` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.ledim_name
CREATE TABLE IF NOT EXISTS `ledim_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection` varchar(50) DEFAULT '0',
  `dateset` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `value` varchar(50) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.mcp4131
CREATE TABLE IF NOT EXISTS `mcp4131` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stage` varchar(50) NOT NULL DEFAULT '0',
  `run` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.piinfo
CREATE TABLE IF NOT EXISTS `piinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kpi` varchar(45) NOT NULL,
  `value` varchar(45) NOT NULL,
  `measurement` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.relay_custom_time
CREATE TABLE IF NOT EXISTS `relay_custom_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `beginning` timestamp NULL DEFAULT NULL,
  `ending` timestamp NULL DEFAULT NULL,
  `gpio` int(11) DEFAULT NULL,
  `intime` int(11) DEFAULT NULL,
  `outtime` int(11) DEFAULT NULL,
  `hold` int(11) DEFAULT NULL,
  `polarity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.relay_dose
CREATE TABLE IF NOT EXISTS `relay_dose` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `polarity` int(11) NOT NULL DEFAULT '1',
  `state` int(11) NOT NULL DEFAULT '0',
  `gpio` int(11) NOT NULL DEFAULT '0',
  `description` varchar(50) DEFAULT NULL,
  `mls` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.relay_dose_sched
CREATE TABLE IF NOT EXISTS `relay_dose_sched` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` int(11) NOT NULL DEFAULT '0',
  `time` time NOT NULL DEFAULT '00:00:00',
  `seconds` float NOT NULL DEFAULT '0',
  `relay_dose_id` int(11) DEFAULT NULL,
  `dosecompleted` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.relay_master
CREATE TABLE IF NOT EXISTS `relay_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `sunrise` int(11) NOT NULL,
  `morning` int(11) NOT NULL,
  `daytime` int(11) NOT NULL,
  `afternoon` int(11) NOT NULL,
  `sunset` int(11) NOT NULL,
  `night` int(11) NOT NULL,
  `nolight` int(11) NOT NULL,
  `gpio` int(11) NOT NULL,
  `auto` int(11) NOT NULL,
  `thermconfig_id` int(11) NOT NULL DEFAULT '0',
  `therm_low_value` int(11) NOT NULL DEFAULT '0',
  `therm_low_decision` int(11) NOT NULL DEFAULT '0',
  `therm_high_value` int(11) NOT NULL DEFAULT '0',
  `therm_high_decision` int(11) NOT NULL DEFAULT '0',
  `state` int(11) NOT NULL DEFAULT '0',
  `polarity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.relay_wave
CREATE TABLE IF NOT EXISTS `relay_wave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `relay` int(11) DEFAULT NULL,
  `gpio` int(11) DEFAULT NULL,
  `polarity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.relay_wave_phase
CREATE TABLE IF NOT EXISTS `relay_wave_phase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL DEFAULT '0',
  `wave_a_pulse` float NOT NULL DEFAULT '0',
  `wave_a_rest` float NOT NULL DEFAULT '0',
  `wave_a_state` varchar(50) DEFAULT NULL,
  `wave_b_pulse` float DEFAULT NULL,
  `wave_b_rest` float DEFAULT NULL,
  `wave_b_state` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.sched
CREATE TABLE IF NOT EXISTS `sched` (
  `id` int(11) NOT NULL,
  `sunrise_start` varchar(50) DEFAULT NULL,
  `sunrise_end` varchar(50) DEFAULT NULL,
  `morning_start` varchar(50) DEFAULT NULL,
  `morning_end` varchar(50) DEFAULT NULL,
  `daytime_start` varchar(50) DEFAULT NULL,
  `daytime_end` varchar(50) DEFAULT NULL,
  `afternoon_start` varchar(50) DEFAULT NULL,
  `afternoon_end` varchar(50) DEFAULT NULL,
  `sunset_start` varchar(50) DEFAULT NULL,
  `sunset_end` varchar(50) DEFAULT NULL,
  `night_start` varchar(50) DEFAULT NULL,
  `night_end` varchar(50) DEFAULT NULL,
  `nolight_start` varchar(50) DEFAULT NULL,
  `nolight_end` varchar(50) DEFAULT NULL,
  `phase` varchar(50) DEFAULT NULL,
  `lastupdate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.schedaction
CREATE TABLE IF NOT EXISTS `schedaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phase` int(11) NOT NULL DEFAULT '0',
  `description` varchar(50) DEFAULT NULL,
  `feed` int(11) NOT NULL DEFAULT '0',
  `feed_result` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.species
CREATE TABLE IF NOT EXISTS `species` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` varchar(10000) DEFAULT '0',
  `image` varchar(50) DEFAULT '0',
  `title` varchar(50) DEFAULT '0',
  `classification` varchar(50) DEFAULT NULL,
  `dateadded` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.thermconfig
CREATE TABLE IF NOT EXISTS `thermconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sensorname` varchar(45) NOT NULL,
  `serialnumber` varchar(45) NOT NULL,
  `current_therm` float NOT NULL,
  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.thermlog
CREATE TABLE IF NOT EXISTS `thermlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_thermconfig` int(11) NOT NULL,
  `sensorname` varchar(45) NOT NULL,
  `reading` float NOT NULL,
  `dateset` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115419 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table jayfish.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(50) NOT NULL DEFAULT '0',
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `email` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
