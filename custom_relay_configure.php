<?php
include "connection.php";
// include "nav.php";
?>
<!-- <meta http-equiv="refresh" content="5"> -->

<style type="text/css">
	input {
		width: 100%;
	}
		.stylemyinput_checkbox {
	min-height: 30px;
	min-width: 30px;		

	}
	.container {
		min-width: 800px !important;	
	}
	th {
		text-align: center;
	}


</style>
	
<?php
$onchange= "onChange=\"this.style.background='#fdff8e';\""; ?>

<?php print $onChange; ?>


<div class="container">
<h5>Add Custom Timed Relay</h5>
<form name = "addcustomrelay" action="submit.php" method="POST">
	<table class="table table-bordered table-striped">
	<tr>
	<thead class="thead-dark">
		<th>Description</th>
		<th>Gpio</th>
		<th>Polarity</th>
		<th>If <font style="color: green;">in</font> zone</th>
		<th>If <font style="color: yellow;">out</font> zone</th>
		<th>Auto</th>
		<th>Begin Zone</th>
		<th>End Zone</th>
		
		</thead>
	</tr>
<input name="option" value = "addcustomrelay" hidden>
<td><input required class="form-control" name="description"></td>
<td><input required type="number" min = "0" max = "40" class="form-control" name="gpio"></td>
<td><input required type="number" min = "0" max = "1" value="0" class="form-control" name="polarity"></td>
<td>
	<select name="ifinzone" class="form-control c">
	<optgroup class="form-control group">
		<option class="" value="1">ON</option>
		<option class="" value="0">OFF</option>
	</optgroup>
	</select>
</td>
<td>
	<select name="ifoutzone" class="form-control">
	<optgroup class="form-control group">
		<option class="" value="1">ON</option>
		<option class="" value="0">OFF</option>
	</optgroup>
	</select>
</td>
<td>
	<select name="pause" class="form-control">
	<optgroup class="form-control group">
		<option class="" value="2">AUTO</option>
		<option class="" value="1">Lock ON</option>
		<option class="" value="0">Lock OFF</option>
	</optgroup>
	</select>
</td>
<td><input required type="time" class="form-control" name="begin"></td>
<td><input required type="time" class="form-control" name="end"></td>
</table>
<p> Polarity = 0 for normal relay operation (default).  For low level relays use 1.
<p><button class="btn btn-info" type="submit">Add</button></p>

</form>

<h5>Edit Existing Custom Timed Relay</h5>

<form action="submit.php" method="POST">
	<table class="table table-bordered table-striped">
	<tr>
	<thead class="thead-dark">
		<th>Description</th>
		<th>Gpio</th>
		<th>Polarity</th>
		<th>If <font style="color: green;">in</font> zone</th>
		<th>If <font style="color: yellow;">out</font> zone</th>
		<th>Auto</th>
		<th>Begin Zone</th>
		<th>End Zone</th>
		<th style="background:red;">X</th>
		</thead>
	</tr>
	<input name="option" value="savecustomrelay"  hidden>
<?php
$relay_array = array();
$stmt = $db->query("SELECT * from relay_custom_time ;");while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$id = $row['id']; array_push($relay_array, $id);
	$description = $row['description'];
	$gpio = $row['gpio'];
	$intime = $row['intime'];
	$outtime = $row['outtime'];
	$hold = $row['hold'];
	$polarity = $row['polarity'];
	$begining = $row['beginning'];
	$ending = $row['ending'];

	$begining = strtotime($begining);
	$begining = date("H:i:s",$begining);
	$ending = strtotime($ending);
	$ending = date("H:i:s",$ending);
	$now = date("H:i:s");
	// $end = date("H:i:s",$end);

	if ($intime == "1") {$intime_select_y="selected";$intime_select_n="";}
	if ($intime == "0") {$intime_select_n="selected";$intime_select_y="";}
	
	if ($outtime == "1") {$outtime_select_y="selected";$outtime_select_n="";}
	if ($outtime == "0") {$outtime_select_y="";$outtime_select_n="selected";}

	if ($hold == "2") {$hold_select_none="selected";$hold_select_y="";$hold_select_n="";}
	if ($hold == "1") {$hold_select_y="selected";$hold_select_n="";$hold_select_none="";}
	if ($hold == "0") {$hold_select_n="";$hold_select_n="selected";$hold_select_none="";}

print '<tr>';
print '<td><input '.$onchange.' name="description'.$id.'" class="form-control" value="'.$description.'"></td>';
print '<td><input  '.$onchange.' required type="number" min = "0" max = "40" name="gpio'.$id.'" class="form-control" value="'.$gpio.'"></td>';
print '<td><input  '.$onchange.' required type="number" min = "0" max = "1" value="'.$polarity.'" class="form-control" name="polarity'.$id.'"></td>';
print '<td>

		<select  '.$onchange.' name="intime'.$id.'" class="form-control">
			<optgroup class="form-control group">
				<option class="" value="1" '.$intime_select_y.'>ON</option>
				<option class="" value="0" '.$intime_select_n.'>OFF</option>
			</optgroup>
		</select>
	   </td>';
print '<td>

<select  '.$onchange.' name="outtime'.$id.'" class="form-control">
			<optgroup class="form-control group">
				<option class="" value="1" '.$outtime_select_y.'>ON</option>
				<option class="" value="0" '.$outtime_select_n.'>OFF</option>
			</optgroup>
		</select>

</td>';
print '<td>

<select  '.$onchange.' name="hold'.$id.'" class="form-control">
			<optgroup class="form-control group">
				<option class="" value="2" '.$hold_select_none.'>AUTO</option>
				<option class="" value="1" '.$hold_select_y.'>Lock ON</option>
				<option class="" value="0" '.$hold_select_n.'>Lock OFF</option>
			</optgroup>
		</select>
</td>';
print '<td><input  '.$onchange.' type="time" name="beginning'.$id.'" class="form-control" value="'.$begining.'"></td>';
print '<td><input  '.$onchange.' type="time" name="ending'.$id.'" class="form-control" value="'.$ending.'"></td>';
print '<td><input  '.$onchange.' class="stylemyinput_checkbox" type="checkbox" name="delete'.$id.'" class="form-control" ></td>';
print '</tr>';

// if (($now >= $begining) && ($now <= $ending)){
//     echo "is between";
// }else{
//     echo "NO GO!";  
// }

};


?>

</table>

<p><button  class="btn btn-success" type="submit">Submit</button></p>
</form><br>

</div>
