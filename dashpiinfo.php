
  <!-- <script src="vendor/bootstrap/js/bootstrap.min.js"></script> -->
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css">
  <link href="dash.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Orbitron&display=swap" rel="stylesheet">
  <!-- <script src="js/jquery.js"></script> -->

    <?php
      exec("free -h | grep Mem | awk '{print $3s}'", $output);
  foreach ($output as $key => $value) {
    $memfree = $value;
  };
  $output="";
  exec("free -h | grep Mem | awk '{print $2s}'", $output);
  foreach ($output as $key => $value) {
    $memtotal = $value;
  };
  $output="";
  exec("df -h | grep root | awk '{print $4}'", $output);
  foreach ($output as $key => $value) {
    $diskfree = $value;
  };
  $output="";
  exec("df -h | grep root | awk '{print $2}'", $output);
  foreach ($output as $key => $value) {
    $totaldisk = $value;
  };

if ($dontopen == 1) {$margin="0%";} else { $margin="10%";};

print '
<div style="margin-top:'.$margin.';"></div>
<div align="center">
<div class="row" style="">
  <div class="col-sm-3"></div>
    <div class="col-sm-6">
    
    <h3><img src="images/pi.png" width="30"> RASPBERRY PI INFO</h3>
      <table class="table table-striped table-dark" style="max-width:600px;">
        <thead>
        <th>Descriptor</th><th>Information</th>
        </thead>
        <tr><td>'.'Ram Free: </td><td>'.$memfree.'B of '.$memtotal.'B</td></tr>
        <tr><td>'.'Disk Free: </td><td>'.$diskfree.' FREE of '.$totaldisk.'</td></tr>
        <tr><td>'.'IP Address: </td><td>'.$_SERVER['SERVER_ADDR'].'</td></tr>
      </table>
      </div>
    </div>
  <div class="col-sm-3"></div>

';
// print '<div align="center">';
if ($dontopen == 1) {;} else { include "dashmenu.php";};
// include "dashmenu.php";
print '</div>';
?>