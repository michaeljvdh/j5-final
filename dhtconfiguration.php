
<!DOCTYPE html>
<html>
<head>
	<title></title>
<?php
              include "connection.php";
              // include 'nav.php';

?>

<style type="text/css">
	/*.stylemyinput {
		max-width: 60px;
		text-align: center;
		display: inline;
	}*/
	.stylemyinput_checkbox {
	min-height: 30px;
	min-width: 30px;		

	}
	table td { padding: 3px !important;	text-align: center;}
	table th { padding: 3px !important; text-align: center;	}
</style>

</head>
<body>
<div class="container">

<?php



?>



<H5>Add a DHT Sensor</H5>
<form id="dhtconfigadd" action="submit.php" method="POST">
	<input name="option" value="dhtconfigadd" hidden>
	<table class="table table-striped table-bordered">
	<tr>
	<thead class="thead-dark">
		<th>PWM</th>
		<th>Channel</th>
		<th>Penalty</th>
		<th>Relay Gpio</th>
		<th>Pivot</th>
		
		
		<th>Dht Gpio</th>
	</thead>
	</tr>
		<td><input type="number" min = "0" max = "1"  class="form-control stylemyinput" name="pwmoveride" required></td>
		<td><input type="number" min = "0" max = "15"  class="form-control stylemyinput" name="channel" required></td>
		<td><input type="number" min = "0" max = "100"  class="form-control stylemyinput" name="pwmpenalty" placeholder="%" required></td>
		<td><input type="number" min = "0" max = "40"  class="form-control stylemyinput" name="relaygpio" required></td>
		<td><input type="number" min = "0" max = "100"  class="form-control stylemyinput" name="hpivot" placeholder="%" required></td>
		
		
		<td><input type="number" min = "0" max = "40"  class="form-control stylemyinput" name="dhtgpio" required></td>
	</table>
<p></p>
<p><button class="btn btn-info" type="submit">Add</button></p>
</form>

<H5>Registered DHT22 Devices</H5>
<?php
$dht_id_array = array(); $stmt = $db->query('SELECT id from dhtconfig;');while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	array_push($dht_id_array,$row['id']); };
print '<form id="dhtconfigsave" action="submit.php" method="POST">';
print '<input name="option" value="dhtconfigsave" hidden>';
print '

	<table class="table table-bordered table-striped">
	<tr>
	<thead class="thead-dark">
		<th>PWM</th>
		<th>Channel</th>
		<th>Penalty %</th>
		<th>Relay Gpio</th>
		<th>Pivot %</th>
		
		
		<th>Dht Gpio</th>
		<th>LR</th>
		<th style="background:red;">X</th>
		</thead>
	</tr>

';
foreach ($dht_id_array as $key => $value) {

$stmt = $db->query("SELECT * FROM dhtlog WHERE id='$value' ORDER BY id DESC LIMIT 1;");
   while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$lastreading = $row['hdht'];
		$lastreading_time = $row ['tstamp'];
		};
		$lastreading_time = strtotime($lastreading_time);
		$new = date("Y - F j, g:i a", $lastreading_time);

print "Last Successful Reading ".$new;


$stmt = $db->query("SELECT * from dhtconfig where id = '$value';");while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$id = $row['id'];
	$channel = $row['channel'];
	$relaygpio = $row['relaygpio'];
	$hpivot = $row['hpivot'];
	$pwmoveride = $row['pwmoveride'];
	$pwmpenalty = $row['pwmpenalty'];
	$dhtgpio = $row['dhtgpio'];	
	print'
	<input class="stylemyinput" name="id'.$value.'" value="'.$value.'" hidden>
	<tr>
	<td><input class="form-control stylemyinput" name="pwmoveride'.$value.'" value="'.$pwmoveride.'"></td>
	<td><input class="form-control stylemyinput" name="channel'.$value.'" value="'.$channel.'"></td>
	<td><input class="form-control stylemyinput" name="pwmpenalty'.$value.'" value="'.$pwmpenalty.'"></td>
	<td><input class="form-control stylemyinput" name="relaygpio'.$value.'" value="'.$relaygpio.'"></td>
	<td><input class="form-control stylemyinput" name="hpivot'.$value.'" value="'.$hpivot.'"></td>
	
	
	<td><input class="form-control stylemyinput" name="dhtgpio'.$value.'" value="'.$dhtgpio.'"></td>
	<td style="padding-top:8px !important;">'.$lastreading.'%</td>
	<td style="padding-top:8px !important;"><input class="form-control stylemyinput_checkbox" type="checkbox" name="delete'.$value.'"></td>
	</tr>
	';
	};

};

print '</table>';
print '<p><button  class="btn btn-success" type="submit">Submit</button></p>';

print '</form>';
?>


</table>
<div class="card" style="width: 97%;">
  <div class="card-body">
    <h5 class="card-title">Legend / Guide</h5>
    <h6 class="card-subtitle mb-2 text-muted">How to configure</h6>
    <p class="card-text">
    	<p><strong>TIP:</strong>You can add multiple lines for the SAME DHT sensor to trigger in different thresholds with differnt GPIO's</p>
    	<p><strong>Channel:</strong> If you intend to use PWM you can use channels from 0-15, make sure that you do not conflict with LDD LED PWM settings. This field is required if you intend not using an LDD Fan - make this 0 and make your PWM 0 - This will effectively mean that you only which to activate Relay if the threshold is exceeded.</p>
    	<p>PWM fan speed is relative to humidity and will increase against humidity as needed.</p>
    	<p><strong>Relay Gpio:</strong> If your humidity Pivot threshold has been exceeded activate this relay / GPIO.  Enter the GPIO you intend to use.</p>
    	<p><strong>Pivot:</strong> Pivot is your threshold, if the threshold is exceeded it will trigger the Relay GPIO if configured.</p>
    	<p><strong>PWM:</strong> Choosing a 0 or 1 to enable or disable PWM use.  You do not have to use the PWM, if you simply wish to trigger a relay without using a PWM fan you can.
    	</p>
    	<p>
    	<strong>Penalty:</strong> When using PWM fans, they are ALL different, in some cases the speed of the fan when the humidity is accepatable might still be to high / noisy, if you enter a penalty % it will reduce it by that.
    	</p>
    	<p>
    	<strong>DhtGPIO</strong>: The GPIO pin you have plugged your sensor into.
    	</p>
    	<p>
    	<strong>LR:</strong> Is the last reading taken.
    	</p>
    	<p>
    	<strong>X:</strong> If you would like to delete the DHT item.
    	</p>

    </p>    
  </div>
</div>
<br>
</div>
</body>
</html>

