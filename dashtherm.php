

<head>
  
  <!-- <link href="dash.css" rel="stylesheet"> -->
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Orbitron&display=swap" rel="stylesheet">
</head>

<div style="margin-left: 15px;">
<?php
include "connection.php";

 $stmt = $db->query('SELECT * FROM codes WHERE code="thermtype";');
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $thermtype=$row['state'];
    };



function bracketbox_type_1_therm($title,$data) {
  print '
  <div class="bracketbox">
    <div class="bracket_title">'.$title.'</div>
    <div id="left">&nbsp;</div>
    <div class="middle"  style="padding-left:15px; padding-right:15px;">'.$data.'&#176;</div> 
    <div id="right">&nbsp;</div>
    <div class="bracket_bottom_title_therm">THERMOSTAT</div>
  </div>
  ';
};

function bracketbox_type_2_dht($title,$data,$pwmpercent,$rpm) {
  print '
  <div class="bracketbox" >
    <div class="bracket_title">'.$title.'</div>
    <div id="left">&nbsp;</div>
    <div class="middle" style="padding-left:25px; padding-right:25px;">'.$data.'%</div> 
    <div id="right">&nbsp;</div>
    <div class="bracket_bottom_title_dht">FAN@<font style=";color:red;">'.$pwmpercent.'% PWM</font> = '.$rpm.'</div>
  </div>
  ';
};

$stmt = $db->query('SELECT * FROM thermconfig;');
                                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                      $title = $row['sensorname'];
                                      $therm = $row['current_therm'];
                                      if ($thermtype=="0") {$therm=$therm*9/5+32;};
                                      bracketbox_type_1_therm($title,$therm);
                                    };

$dhts = array();
$stmt = $db->query('SELECT * FROM dhtconfig;');
                                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                      $id=$row['id'];
                                      array_push($dhts, $id);
                                      };

foreach ($dhts as $key => $value) {

$stmt = $db->query("SELECT * FROM dhtlog WHERE dhtconfig_id='$value' ORDER BY id DESC LIMIT 1;");
                                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                      $dhtconfig_id = $row['dhtconfig_id'];
                                      $humidity = $row['hdht'];
                                      $pwmpercent = $row['pwmpercent'];                                      
                                      $stmtb = $db->query("SELECT * FROM dhtconfig WHERE id='$dhtconfig_id';");
                                      while($rowb = $stmtb->fetch(PDO::FETCH_ASSOC)) { 
                                        $name = $rowb['name']; 
                                        $rpm = $rowb['pwmrate'];
                                        }; 
                                        // $rpm=1000;
                                      bracketbox_type_2_dht($name,$humidity,$pwmpercent,$rpm);
                                    };

};

?>
</div>