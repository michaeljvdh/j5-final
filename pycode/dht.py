import Adafruit_DHT
import time
import MySQLdb
import RPi.GPIO as GPIO
import dbconnection
import datetime

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

# GPIO.setup(int(gpio), GPIO.OUT)
# GPIO.output(int(gpio), truestate)

dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()

sensor = Adafruit_DHT.DHT22

debug = 0
relaygpio= 0
state= 0
count = 1
x=1

db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)
curs = db.cursor()


while x == 1:
    dhtarray = []
    if debug == 1:
        print "------------------------In main loop-----------------------"
    curs.execute ('SELECT * FROM dhtconfig')
    results = curs.fetchall()
    for row in results:
        id = (row[0])
        dhtarray.append (id)   

    for object in dhtarray:
        curs.execute ('SELECT * FROM dhtconfig where id=%s' % (object))
        if debug ==1:
            print "Processing ID:",object        
        results = curs.fetchall()
        for row in results:
            dhtgpio = (row[9])
            dhtconfigid = (row[0])    
        if dhtgpio == 3:    pin = 2 
        if dhtgpio == 5:    pin = 3
        if dhtgpio == 7:    pin = 4
        if dhtgpio == 8:    pin = 14
        if dhtgpio == 10:    pin = 15
        if dhtgpio == 11:    pin = 17
        if dhtgpio == 12:    pin = 18
        if dhtgpio == 13:    pin = 27
        if dhtgpio == 15:    pin = 22
        if dhtgpio == 16:    pin = 23
        if dhtgpio == 18:    pin = 24
        if dhtgpio == 19:    pin = 10
        if dhtgpio == 21:    pin = 9
        if dhtgpio == 22:    pin = 25
        if dhtgpio == 23:    pin = 11
        if dhtgpio == 24:    pin = 8
        if dhtgpio == 26:    pin = 7
        if dhtgpio == 29:    pin = 5
        if dhtgpio == 31:    pin = 6
        if dhtgpio == 32:    pin = 12
        if dhtgpio == 33:    pin = 13
        if dhtgpio == 35:    pin = 19
        if dhtgpio == 36:    pin = 16
        if dhtgpio == 37:    pin = 26
        if dhtgpio == 38:    pin = 20
        if dhtgpio == 40:    pin = 21        
        if dhtgpio > 0:        
            try:
                if debug == 1:
                    print "Trying to read from Sensor"
                humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
                if humidity is not None and temperature is not None:
                    count = count + 1
                    # if count == 17280:
                    now = datetime.datetime.now()
                    now = (now.strftime("%H:%M:%S"))
                    # print now
                    if now > "00:00:00" and now < "00:00:30":                    
                        try:                                                     
                            curs.execute ('TRUNCATE TABLE `dhtlog`;')
                            #curs.execute ('ALTER TABLE `dhtlog` DROP `id`;')
                            #curs.execute ('ALTER TABLE `dhtlog` ADD  `id` INT(11) NOT NULL AUTO_INCREMENT FIRST ,ADD PRIMARY KEY (`id`);')
                            #curs.execute ('DELETE FROM `dhtlog`  WHERE id <= ( SELECT id  FROM ( SELECT id   FROM `dhtlog`  ORDER BY id DESC  LIMIT 1 OFFSET 10 ) foo )')
                            count = 1        
                        except:
                            donothing = 1
            
                    # print('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity))
                    tdht = '{0:0.1f}' .format(temperature)
                    hdht = '{0:0.1f}' .format(humidity)
                    tdht = float(tdht)
                    tdht = int(tdht)
                    hdht = float(hdht)    
                    hdht = int(hdht)    
                    # curs.execute ('INSERT INTO dhtlog SET tdht="%s", hdht="%s", dhtconfig_id="%s"'%(tdht,hdht,dhtconfigid))
                    # db.commit()
                  
                    curs.execute ('SELECT * FROM dhtconfig where id="%s"' %(dhtconfigid))
                    results = curs.fetchall()
                    for row in results:
            
                        tpivot = (row[3])
                        hpivot = (row[4])
                        pwmpenalty = (row[8])
                    if debug==1:
                        print ("temperature- pivot:") , tpivot, "vs ", tdht
                        print ("humidity---- pivot:") , hpivot, "vs ", hdht
            
                    if debug == 1:
                        print "-----------------------------"
                        print "humidity=",hdht
                        print "therm =", tdht
                        if tdht > tpivot:
                    	   print 'low temp',tpivot
                        if tdht < tpivot:
                        	print 'high temp',tpivot
                        if hdht >= hpivot:    
                        	print 'low humid',hpivot
                        if hdht <= hpivot:
                        	print 'high humid',hpivot
                        print "-----------------------------"
                    
            
                    curs.execute ('SELECT * FROM dhtconfig where id ="%s"' % (dhtconfigid))
                    results = curs.fetchall()
                    for row in results:
                        pwmchannel = (row[1])
                        relaygpio = (row[2])
                        pwmrate = (row[6])
                        pwmoveride = (row[7])
                    if pwmoveride == 1: #This means address I2C PCA9685
                        calc = int(hdht)
                        calcresult = 4096 * calc / 100
                        pwmpenalty_calc = pwmpenalty * 4096 / 100
                        if debug==1:
                        	print "Penalty:",pwmpenalty_calc
                        calcresult = int(calcresult) - pwmpenalty_calc
                        if calcresult < 1:
                        	calcresult = 0
                        if calcresult > 4096:
                        	calcresult = 4096 
                        if debug == 1:
                            print "calc=",calcresult                       

                        # if hdht<=hpivot:
                        #     pwmpenalty_calc = pwmpenalty * 4096 / 100
                        #     if debug==1:
                        #         print "Penalty:",pwmpenalty_calc
                        #     calcresult = int(calcresult) - pwmpenalty_calc
                        #     if calcresult < 1:
                        #         calcresult = 0
                        #     if calcresult > 4096:
                        #         calcresult = 4096
                        # else:
                        #     calcresult = int(calcresult)
                        pwmpercent = calcresult * 100 / 4096
                        curs.execute ('INSERT INTO dhtlog SET pwmpercent="%s",tdht="%s", hdht="%s", dhtconfig_id="%s"' % (pwmpercent,tdht,hdht,dhtconfigid))
                        db.commit()                        
                        curs.execute ("UPDATE dhtconfig SET pwmrate='%s' where id='%s'" % (calcresult,dhtconfigid))
                        if debug == 1:
                            print "****** UPDATING ******:", dhtconfigid, "WITH", calcresult
                        db.commit()
                        # EVEN IN PWM MODE it will allow you to react relay too ----- START
                        if hdht > hpivot: #SET RELAY TO ON
                            curs.execute ("UPDATE dhtconfig SET relaystate='1' where id='%s'") %(dhtconfigid)
                            db.commit()
                            if relaygpio == 0:
                                donothing = 1
                            else:
                                GPIO.setup(int(relaygpio), GPIO.OUT)
                                GPIO.output(int(relaygpio), 1)
                        if hdht < hpivot: #SET RELAY TO OFF
                            curs.execute ("UPDATE dhtconfig SET relaystate='0' where id='%s'") %(dhtconfigid)
                            db.commit()
                            if relaygpio == 0:
                                donothing = 1
                            else:
                                GPIO.setup(int(relaygpio), GPIO.OUT)
                                GPIO.output(int(relaygpio), 0)
                        # EVEN IN PWM MODE it will allow you to react relay too ----- END
            
                        if debug ==1:
                            print "pulse rate:", pwmrate
                    else: #This means PCA9685 is not present and to set relay trigger instead
                        if debug==1:
                            print "Executing Non PCA Section"
                        curs.execute ('INSERT INTO dhtlog SET pwmpercent="0",tdht="%s", hdht="%s", dhtconfig_id="%s"' % (tdht,hdht,dhtconfigid))
                        db.commit()
                        curs.execute ("UPDATE dhtconfig SET pwmrate='0' where id='%s'" % (dhtconfigid))
                        db.commit()
                        if hdht > hpivot: #SET RELAY TO ON
                            curs.execute ("UPDATE dhtconfig SET relaystate='1' where id='%s'") %(dhtconfigid)
                            db.commit()
                            if relaygpio == 0:
                                donothing = 1
                            else:
                                GPIO.setup(int(relaygpio), GPIO.OUT)
                                GPIO.output(int(relaygpio), 1)
                        if hdht < hpivot: #SET RELAY TO OFF
                            curs.execute ("UPDATE dhtconfig SET relaystate='0' where id='%s'") %(dhtconfigid)
                            db.commit()
                            if relaygpio == 0:
                                donothing = 1
                            else:
                                GPIO.setup(int(relaygpio), GPIO.OUT)
                                GPIO.output(int(relaygpio), 0)
                    time.sleep(1)
                    if debug == 1:
                        print "Failes"
                        donothing = 0

                    
                else:
                    if debug ==1:
                        print "Reading Failed - will try again"
                    donothing = 0
            except:
                donothing = 0
                if debug == 1:
                    print "Failed"