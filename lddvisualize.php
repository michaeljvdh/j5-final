<html>
   <?php
        // include "nav.php";
        include "connection.php";
   ?>

<style type="text/css">
	.progress {
		height: 25px;
	}
	table td {
		padding: 4px !important;
	}
</style>

<head>
	<?php
	$namearray = array();
	$sunriseendarray= array();
	$morningendarray= array();
	$daytimeData = array();
	$afternoonData = array();
	$sunsetData = array();
	$nightData =array();
	$nolightData = array();
	$stmt = $db->query('SELECT name,end,ledim_name_id,phase FROM ledim_name A RIGHT JOIN ledim B ON A.id = B.ledim_name_id WHERE phase="sunrise"');
                                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

                                    $name=$row['name'];
                                    $end = $row['end'];
                                    $end = $end * 100 / 4096;
                                    $end = round($end);
                                    array_push($namearray, $name);
                                    array_push($sunriseendarray, $end);                                   
                                    };
	
    $stmt = $db->query('SELECT name,end,ledim_name_id,phase FROM ledim_name A RIGHT JOIN ledim B ON A.id = B.ledim_name_id WHERE phase="morning"');
    	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { $end = $row['end'];$end = $end * 100 / 4096;$end = round($end);array_push($morningendarray, $end);};

    $stmt = $db->query('SELECT name,end,ledim_name_id,phase FROM ledim_name A RIGHT JOIN ledim B ON A.id = B.ledim_name_id WHERE phase="daytime"');
    	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { $end = $row['end'];$end = $end * 100 / 4096;$end = round($end); array_push($daytimeData, $end);};

    $stmt = $db->query('SELECT name,end,ledim_name_id,phase FROM ledim_name A RIGHT JOIN ledim B ON A.id = B.ledim_name_id WHERE phase="afternoon"');
    	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { $end = $row['end'];$end = $end * 100 / 4096;$end = round($end); array_push($afternoonData, $end);};

    $stmt = $db->query('SELECT name,end,ledim_name_id,phase FROM ledim_name A RIGHT JOIN ledim B ON A.id = B.ledim_name_id WHERE phase="sunset"');
    	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { $end = $row['end'];$end = $end * 100 / 4096;$end = round($end); array_push($sunsetData, $end);};

    $stmt = $db->query('SELECT name,end,ledim_name_id,phase FROM ledim_name A RIGHT JOIN ledim B ON A.id = B.ledim_name_id WHERE phase="night"');
    	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { $end = $row['end'];$end = $end * 100 / 4096;$end = round($end); array_push($nightData, $end);};

    $stmt = $db->query('SELECT name,end,ledim_name_id,phase FROM ledim_name A RIGHT JOIN ledim B ON A.id = B.ledim_name_id WHERE phase="nolight"');
    	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { $end = $row['end'];$end = $end * 100 / 4096;$end = round($end); array_push($nolightData, $end);};

	?>

<div style="padding-left: 2%;padding-right: 2%;">
<table class="table table-dark">
	<?php
print '<th></th>';
	foreach ($namearray as $key => $value) {
		print '<th>'.$value.'</th>';
		# code...
	};

print '<tr>';
print '<td>Sunrise</td>';
	foreach ($sunriseendarray as $key => $value) {
		print '<td><div class="progress"><div class="progress-bar" role="progressbar" style="width: '.$value.'%"><div style="color:black;">'.$value.'%</div></div></div></td>';
		# code...
	};
print '</tr>';

print '<tr>';
print '<td>Morning</td>';
	foreach ($morningendarray as $key => $value) {
		print '<td><div class="progress"><div class="progress-bar" role="progressbar" style="width: '.$value.'%"><div style="color:black;">'.$value.'%</div></div></div></td>';
		# code...
	};
print '</tr>';

print '<tr>';
print '<td>Daytime</td>';
	foreach ($daytimeData as $key => $value) {
		print '<td><div class="progress"><div class="progress-bar" role="progressbar" style="width: '.$value.'%"><div style="color:black;">'.$value.'%</div></div></div></td>';
		# code...
	};
print '</tr>';

print '<tr>';
print '<td>Afternoon</td>';
	foreach ($afternoonData as $key => $value) {
		print '<td><div class="progress"><div class="progress-bar" role="progressbar" style="width: '.$value.'%"><div style="color:black;">'.$value.'%</div></div></div></td>';
		# code...
	};
print '</tr>';

print '<tr>';
print '<td>Sunset</td>';
	foreach ($sunsetData as $key => $value) {
		print '<td><div class="progress"><div class="progress-bar" role="progressbar" style="width: '.$value.'%"><div style="color:black;">'.$value.'%</div></div></div></td>';
		# code...
	};
print '</tr>';

print '<tr>';
print '<td>Night</td>';
	foreach ($nightData as $key => $value) {
		print '<td><div class="progress"><div class="progress-bar" role="progressbar" style="width: '.$value.'%"><div style="color:black;">'.$value.'%</div></div></div></td>';
		# code...
	};
print '</tr>';

print '<tr>';
print '<td>Nolight</td>';
	foreach ($nolightData as $key => $value) {
		print '<td><div class="progress"><div class="progress-bar" role="progressbar" style="width: '.$value.'%"><div style="color:black;">'.$value.'%</div></div></div></td>';
		# code...
	};
print '</tr>';
	?>

</table>
<p style="color:white;">
	The Ldd Visualizer helps you see what your settings look like across the board, to help you balance them more appropriately.
</p>
<button type="button" class="btn btn-warning" onclick="location.href='index.php?page=lddlights'">Back to PWM</button>
</div>

	
</body>



</html>

