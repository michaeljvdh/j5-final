<!DOCTYPE html>
<html>

	<title></title>

<?php 
// include "nav.php";
// include "connection.php";
$dontopen=1;
 ?>
<head>
  
  <!-- <script src="vendor/bootstrap/js/bootstrap.min.js"></script> -->
  <!-- <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css"> -->
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Orbitron&display=swap" rel="stylesheet">
  <!-- <script src="js/jquery.js"></script> -->
  <!-- <link href="dash.css" rel="stylesheet"> -->
</head>

<style type="text/css">
  table td{
    margin:-1px !important;
    padding: 1px !important;
  }
  
html {
    background-color:#1a1a1a !important;
    /*background-color: red !important;*/

  }

  body {
    background-color:#1a1a1a !important;
    /*background-color: red !important;*/

  }

</style>

<br><br>
<body>
<div align="center">

  The Pi LCD Dashboard URL can be found <a href="dash.php">HERE</a>
  Reboot and shutdown can be actioned from the Pi LCD Dash Menu
<div class="row" style="">
  <div class="col-sm-12" style="">
    <div class="dashtherm"><img src="images/loading.gif"></div>
  </div>
</div>

<div class="row" style="">
  <div class="col-sm-12" style="">
    <div class="dashato" style="display: inline-block;"><img src="images/loading.gif"></div>
    <div class="dashchange" style="display: inline-block;"><img src="images/loading.gif"></div>
  </div>
</div>



<div class="row" style="">
  <div class="col-sm-12" style="">
   <div class="dashbrick"><img src="images/loading.gif"></div> 
  </div>
</div>

<?php
include "dashpiinfo.php";
include "dash-manual-relays.php";
// include "dashdosefull.php";
?>


</div>
<br><br>

</body>
</html>

<!-- ****** PULL STATE CONSTANTLY ****** -->
<script type="text/javascript">
  refreshdashtherm();
  setInterval( refreshdashtherm, 2000 );
  var inRequestc = false;
  function refreshdashtherm() {
    if ( inRequestc ) {
      return false;
    }
    inRequestc = true;
    var load = $.get('dashtherm.php');
    $(".dashtherm");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".dashtherm").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequestc = false;
    });
  }
</script>

<!-- ****** PULL STATE CONSTANTLY ****** -->
<script type="text/javascript">
  refreshdashbrick();
  setInterval( refreshdashbrick, 3000 );
  var inRequestb = false;
  function refreshdashbrick() {
    if ( inRequestb ) {
      return false;
    }
    inRequestb = true;
    var load = $.get('dashbrick.php');
    $(".dashbrick");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".dashbrick").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequestb = false;
    });
  }
</script>



<!-- ****** PULL STATE CONSTANTLY ****** -->
<script type="text/javascript">
  refreshato();
  setInterval( refreshato, 2500 );
  var inRequestd = false;
  function refreshato() {
    if ( inRequestd ) {
      return false;
    }
    inRequestd = true;
    var load = $.get('dashato.php');
    $(".dashato");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".dashato").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequestd = false;
    });
  }
</script>

<!-- ****** PULL STATE CONSTANTLY ****** -->
<script type="text/javascript">
  refreshdashchange();
  setInterval( refreshdashchange, 5000 );
  var inRequeste = false;
  function refreshdashchange() {
    if ( inRequeste ) {
      return false;
    }
    inRequeste = true;
    var load = $.get('dashchange.php');
    $(".dashchange");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".dashchange").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequeste = false;
    });
  }
</script>



<!-- ****** PULL STATE CONSTANTLY ****** -->
<!-- <script type="text/javascript">
  refreshlastdose();
  setInterval( refreshlastdose, 4500 );
  var inRequestf = false;
  function refreshlastdose() {
    if ( inRequestf ) {
      return false;
    }
    inRequestf = true;
    var load = $.get('dashdose.php');
    $(".lastdose");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".lastdose").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequestf = false;
    });
  }
</script> -->

<!-- ****** PULL STATE CONSTANTLY ****** -->
<!-- <script type="text/javascript">
  refreshlastrss();
  setInterval( refreshlastrss, 3500 );
  var inRequestg = false;
  function refreshlastrss() {
    if ( inRequestg ) {
      return false;
    }
    inRequestg = true;
    var load = $.get('dashrss.php');
    $(".lastrss");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".lastrss").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequestg = false;
    });
  }
</script> -->
