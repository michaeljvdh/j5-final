 <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
  <link href="dash.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Orbitron&display=swap" rel="stylesheet">
  <script src="js/jquery.js"></script>


<?php
include "dashmenu.php";
include "connection.php";



$option = $_POST['option'];
$answer = $_POST['answer'];
// print $option;

// print $answer;

// if ($option == ""){
// };


if ($option == "rss"){
	
	print '<br><Br>';
	print '<div align="center">';
	// print '<div class="row">';
	print 'Are you sure you want to drop all rss records older than today?';
	// print '</div>';
	// print '<div class="row">';
	print '<br><Br>';
	print '<form action="dash-submit.php" method="post">';
	print '<input name="option" value="rss" hidden>';
	print '<button class="btn btn-action btn-success" name="answer" value="yes">YES</button>';
	print '<button class="btn btn-action btn-danger"  name="answer" value="no">NO</button>';
	print '</form>';	
	print '</div>';
	if (isset($answer)) {
						if ($answer=="yes"){
								print '<div align="center">';
								print "Clearing Records";
								print '<img src="/images/loading.gif">';
					$stmt = $db->query('DELETE FROM `alert` WHERE DATE(`dateset_timeset`) <> CURDATE();');
                                    
								print '<meta http-equiv="refresh" 								
								content="2;url=dashrss.php?page=customrelayconfigure"/>';					
								print '</div>';
							} 
							else							
							{
								print '<meta http-equiv="refresh" 								
								content="0;url=dashrss.php?page=customrelayconfigure"/>';
							};
						};

};



if ($option == "waterchange"){

	print '<br><Br>';
	print '<div align="center">';
	// print '<div class="row">';
	print 'Are you sure you want to RESET waterchange counter ?';
	// print '</div>';
	// print '<div class="row">';
	print '<br><Br>';
	print '<form action="dash-submit.php" method="post">';
	print '<input name="option" value="waterchange" hidden>';
	print '<button class="btn btn-action btn-success" name="answer" value="yes">YES</button>&nbsp;';
	print '<button class="btn btn-action btn-danger"  name="answer" value="no">NO</button>';
	print '</form>';	
	print '</div>';
	if (isset($answer)) {
						if ($answer=="yes"){
								print '<div align="center">';
								print "Resetting to start today";
								print '<img src="/images/loading.gif">';
					$stmt = $db->query('UPDATE event SET dateset = CURDATE() WHERE event="waterchange";');
                                    
								print '<meta http-equiv="refresh" 								
								content="2;url=dashaction.php?page=customrelayconfigure"/>';					
								print '</div>';
							} 
							else							
							{
								print '<meta http-equiv="refresh" 								
								content="0;url=dashaction.php?page=customrelayconfigure"/>';
							};
						};

};


if ($option == "shutdown"){

	print '<br><Br>';
	print '<div align="center">';
	// print '<div class="row">';
	print 'Are you sure you want to Shutdown ?';
	// print '</div>';
	// print '<div class="row">';
	print '<br><Br>';
	print '<form action="dash-submit.php" method="post">';
	print '<input name="option" value="shutdown" hidden>';
	print '<button class="btn btn-action btn-success" name="answer" value="yes">YES</button>&nbsp;';
	print '<button class="btn btn-action btn-danger"  name="answer" value="no">NO</button>';
	print '</form>';	
	print '</div>';
	if (isset($answer)) {
						if ($answer=="yes"){
								print '<div align="center">';
								print "MESSAGE";
								print '<img src="/images/loading.gif">';
					$stmt = $db->query('UPDATE admin SET value="1" WHERE setting="shutdown";');
                                    
								print '<meta http-equiv="refresh" 								
								content="2;url=dashaction.php?page=customrelayconfigure"/>';					
								print '</div>';
							} 
							else							
							{
								print '<meta http-equiv="refresh" 								
								content="0;url=dashaction.php?page=customrelayconfigure"/>';
							};
						};

};


if ($option == "reboot"){

	print '<br><Br>';
	print '<div align="center">';
	// print '<div class="row">';
	print 'Are you sure you want to Reboot ?';
	// print '</div>';
	// print '<div class="row">';
	print '<br><Br>';
	print '<form action="dash-submit.php" method="post">';
	print '<input name="option" value="reboot" hidden>';
	print '<button class="btn btn-action btn-success" name="answer" value="yes">YES</button>&nbsp;';
	print '<button class="btn btn-action btn-danger"  name="answer" value="no">NO</button>';
	print '</form>';	
	print '</div>';
	if (isset($answer)) {
						if ($answer=="yes"){
								print '<div align="center">';
								print "MESSAGE";
								print '<img src="/images/loading.gif">';
					$stmt = $db->query('UPDATE admin SET value="1" WHERE setting="reboot";');
                                    
								print '<meta http-equiv="refresh" 								
								content="2;url=dashaction.php?page=customrelayconfigure"/>';					
								print '</div>';
							} 
							else							
							{
								print '<meta http-equiv="refresh" 								
								content="0;url=dashaction.php?page=customrelayconfigure"/>';
							};
						};

};




if ($option == "atodashpause"){

	print '<br><Br>';
	print '<div align="center">';
	 
	// print '<div class="row">';
	
	// print '</div>';
	// print '<div class="row">';
	print '<br><Br>';
	print '<form action="dash-submit.php" method="post">';

	print '<input name="option" value="atodashpause" hidden>';
	print '<button class="btn btn-action btn-danger" name="answer" value="yes">DISABLE ATO</button>&nbsp;';
	print '<button class="btn btn-action btn-success"  name="answer" value="no">ENABLE ATO</button>';
	print '</form>';	
	print '</div>';
	if (isset($answer)) {
						if ($answer=="yes"){
								print '<div align="center">';
								
								// print "MESSAGE";
								// print '<img src="/images/loading.gif">';
					$stmt = $db->query("UPDATE admin set value='0' WHERE setting='ato';");
					print '<div align="center"><h3>ATO <font style="color:red;">Halted</font></h3>
					<a href="dashaction.php"><button class="btn btn-action btn-info">RETURN</button></a></div>';
                                    
					// print '<meta http-equiv="refresh" content="2;url=dashaction.php?page=customrelayconfigure"/>';					
								print '</div>';
							} 
							else							
							{
								
								$stmt = $db->query("UPDATE admin set value='1' WHERE setting='ato';");
								print '<div align="center"><h3>ATO <font style="color:lightgreen;">Resumed</font></h3>
								<a href="dashaction.php"><button class="btn btn-action btn-info">RETURN</button></a></div>
								';
								

							};

						};

};


?>

	











<?php
// if ($option == "waterchange"){

// 	print '<br><Br>';
// 	print '<div align="center">';
// 	// print '<div class="row">';
// 	print 'Are you sure you want to XXXXX ?';
// 	// print '</div>';
// 	// print '<div class="row">';
// 	print '<br><Br>';
// 	print '<form action="dash-submit.php" method="post">';
// 	print '<input name="option" value="waterchange" hidden>';
// 	print '<button class="btn btn-action btn-success" name="answer" value="yes">YES</button>&nbsp;';
// 	print '<button class="btn btn-action btn-danger"  name="answer" value="no">NO</button>';
// 	print '</form>';	
// 	print '</div>';
// 	if (isset($answer)) {
// 						if ($answer=="yes"){
// 								print '<div align="center">';
// 								print "MESSAGE";
// 								print '<img src="/images/loading.gif">';
// 					// $stmt = $db->query('DELETE FROM `alert` WHERE DATE(`dateset_timeset`) <> CURDATE();');
                                    
// 								print '<meta http-equiv="refresh" 								
// 								content="2;url=dashaction.php?page=customrelayconfigure"/>';					
// 								print '</div>';
// 							} 
// 							else							
// 							{
// 								print '<meta http-equiv="refresh" 								
// 								content="0;url=dashaction.php?page=customrelayconfigure"/>';
// 							};
// 						};

// };
?>